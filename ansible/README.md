# Ansible

## Add user

First generate a hash of the password you want to use for the user account. The Python package `passlib` is required.

```sh
python3 -c "from passlib.hash import sha512_crypt; import getpass; print(sha512_crypt.using(rounds=5000).hash(getpass.getpass()))"
```

Then edit the encypted file.

```sh
ansible-vault edit inventories/cloudvps/group_vars/all/common_users.yaml
```
