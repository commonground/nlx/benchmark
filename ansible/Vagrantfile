hosts = %w(dash01 dir01 inway01 outway01 txlog01 benchmark-api01 benchmark-client01)

Vagrant.configure("2") do |config|

  config.vm.network "forwarded_port", guest: 80, host: 8080, auto_correct: true
  config.vm.box = "debian/contrib-buster64"

  config.vm.provider "virtualbox" do |vb|
    vb.linked_clone = true
    vb.cpus = 2
    vb.memory = 1024
    vb.default_nic_type = "virtio"

    # Disable the creation of the 'cloudimg-console.log' file
    # See: https://bugs.launchpad.net/cloud-images/+bug/1627844
    vb.customize [ "modifyvm", :id, "--uartmode1", "file", "/dev/null" ]
  end

  hosts.each do |hostname|
    config.vm.define hostname do |machine|
      machine.vm.hostname = "#{hostname}.nlx.local"
    end
  end

  #
  # Run Ansible from the Vagrant Host
  #

  config.vm.provision "ansible" do |ansible|
    ansible.compatibility_mode = "2.0"
    ansible.host_key_checking = false
    # ansible.verbose = "vvvv"
    ansible.playbook = "benchmark.yaml"
    ansible.raw_arguments = [
      "--diff"
    ]

    ansible.vault_password_file = File.join(Dir.getwd, ".vault-password")
    ansible.extra_vars = {
      ansible_python_interpreter: "/usr/bin/python3",
      ansible_ssh_args: "-o ForwardAgent=yes",
      global_nlx_version: "v0.76.0",
    }

    #ansible.start_at_task = 'Refresh facts'

    ansible.groups = {
        "dashboard" => %w(dash01),
        "directory" => %w(dir01),
        "inway" => %w(inway01),
        "outway" => %w(outway01),
        "transaction_log" => %w(txlog01),
        "benchmark_api" => %w(benchmark-api01),
        "benchmark_client" => %w(benchmark-client01),
    }
  end

end
