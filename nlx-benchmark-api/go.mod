module go.nlx.io/nlx-bench

go 1.12

require (
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/pkg/errors v0.8.1 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.10.0
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7 // indirect
)
