# NLX benchmark

Demonstrate a minimal but complete nlx network.

Minimum set of services:

- directory-registration-api
- directory-inspection-api
- database (for the directory)
- inway (to expose an api in the nlx network)
- outway (to consume data from api's out in the nlx network)
- benchmark api which only returns `200 OK`.
- *configure your own api in the inway!*

Benchmarking your own API behind NLX inway is
possible. Just add your service to the
`service-config.toml` the configuration


## Requirements

* [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
* [Docker Compose](https://docs.docker.com/compose/install/)
* [JMeter](https://jmeter.apache.org/)
* ApacheBench

## Usage

```bash
# Generate certificates
ansible-playbook ansible/tls.yaml

# Start containers
docker-compose up

# Send request to outway
curl http://127.0.0.1:8071/nlx/benchmark/

# Run a benchmark, 4 concurrent, 2000 requests
ab -c 4 -n 2000 http://127.0.0.1:8071/nlx/benchmark/

# Run a benchmark, 4 concurrent for 60 seconds
ab -c 4 -t 60 http://127.0.0.1:8071/nlx/benchmark/

# The API will respond after 20ms
ab -c 4 -t 60 http://127.0.0.1:8071/nlx/benchmark/sleep/20ms
```

### JMeter

In the `jmeter` directory you can find a test plan to get started.


### Run outway and inway on the host

Install the outway and inway with `go install`
from [commonground/nlx/nlx](https://gitlab.com/commonground/nlx/nlx). 
Then use `start-inway.sh` and `start-outway.sh`
to start an outway and inway on the host.
